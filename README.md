# Proyecto: Clon de la página de conectate.
### Autor: Junior Cabral.
### Descripción
Realización de un clon de la página de conectate, con el fin de practicar el uso de react y bootstrap.
### Instalaciones necesarias
- ```npm install``` para instalar las dependencias necesarias.
- ```npm install bootstrap``` para instalar bootstrap. 
- ```npm start``` para iniciar el servidor.